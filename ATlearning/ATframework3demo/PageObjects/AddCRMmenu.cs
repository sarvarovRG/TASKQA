﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class AddCRMmenu
    {
        internal NewContact WriteNameContact(string NameContact)
        {
            
                var switchToAddContactFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Переход на фрейм  создания контакта");
                switchToAddContactFrame.SwitchToFrame();

                var ContactName = new WebItem("//input[@id='name_text']", "Поле ввода имени");
                    ContactName.SendKeys(NameContact);
                
            return new NewContact();
            }
        }
    }
