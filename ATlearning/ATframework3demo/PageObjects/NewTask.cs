﻿using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class NewTask
    {
        internal NewTask NameTask()
        {// назначим название задачи "новая задача"
            var TaskTitle = new WebItem("//input[@data-bx-id='task-edit-title']", "Названия задачи");
            TaskTitle.SendKeys("Задача на проверку крайнего срока");
            return new NewTask();
        }

        internal NewTask saveTask()
        {// нажать "поставить задачу"

            var btnSaveTask = new WebItem("//button[@data-bx-id='task-edit-submit' and @class='ui-btn ui-btn-success']", "Кнопка сохранения задачи");
            btnSaveTask.Click();
            

            return new NewTask(); 
        }
    }
}