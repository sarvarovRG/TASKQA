﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class selectedTaskPage
    {
        internal selectedTaskPage EditingDeadline()
        {  // замена крайнего срока
            var TaskFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Фрейм задачи");
            TaskFrame.SwitchToFrame();

            var clickDeadline = new WebItem("//span[@id='task-detail-deadline']",
                "клик на кнопку изменения крайнего срока");

            clickDeadline.Click();

            var clickDeadlineSetup = new WebItem("//div[@class='bx-calendar-set-time-wrap bx-calendar-set-time-wrap-simple']",
              "кнопка Установить время");

            if (!clickDeadlineSetup.WaitElementDisplayed())
            { // установить время  нажата

                var clickDeadlineDate = new WebItem("//a[@class='bx-calendar-button bx-calendar-button-select']",
                 "кнопка ВЫБРАТЬ в Установить время");

                clickDeadlineDate.Click();

                Log.Info("Установить время нажал ,выбрать нажал");

                

            }
            else
            {
                // установить время не  нажата

                clickDeadlineSetup.Click();
                Log.Info("Установить время была не активна, нажал");
                
                var clickDeadlineDate = new WebItem("//a[@class='bx-calendar-button bx-calendar-button-select']",
                 "кнопка ВЫБРАТЬ в Установить время");
                clickDeadlineDate.Click();

            }

          

            return new selectedTaskPage();

        }

        internal bool CheckDeadline (string Deadline)
        {// проверка крайнего срока

            var clickDeadline = new atFrameWork2.SeleniumFramework.WebItem("//span[@id='task-detail-deadline']",
                "Cтатус крайнего срока");
           
            
            return clickDeadline.AssertTextContains(Deadline, "Крайний срок поменялся");
        }
    }
}