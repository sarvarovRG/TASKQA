﻿using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    public class SearchTextBox
    {
        internal selectedContactList FindContactCRM()
        {
            var SelectTitle = new WebItem("//input[@class='header-search-input']", "вписать в поиск Контакты");
            SelectTitle.SendKeys("Контакты");

            SelectTitle.WaitElementDisplayed(10);
            // клик в найденный пункт "Контакты"
            var btnListContact = new WebItem("//div[@title='CRM -> Контакты']", "Список контактов");
            btnListContact.Click();

            return new selectedContactList();
        }
    }
}