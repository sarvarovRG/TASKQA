﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class selectedContactList
    {
        internal AddCRMmenu addNewContact()
        {
            // добавим новый контакт
                var btnAddContact = new WebItem("//a[@class='ui-btn ui-btn-success ui-btn-icon-add crm-btn-toolbar-add']", "Кнопка добавления контакта");
            btnAddContact.Click();


                return new AddCRMmenu();
            }
            
        }
    }
