﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class NewContact
    {
        internal NewContact SaveContact()
        {
            var btnSaveContact = new WebItem("//button[@class='ui-btn ui-btn-success']", "Кнопка сохранить");

                btnSaveContact.Click();
        
            return new NewContact();
        }

        internal СRMContactsPage СloseContactFrame()
        {
            {// перезагрузим стр. 
                DriverActions.Refresh();
                // через поиск вернемся в раздел контакты

                var SelectTitle = new WebItem("//input[@class='header-search-input']", "вписать в поиск Контакты");
                SelectTitle.WaitElementDisplayed(5);
                SelectTitle.SendKeys("Контакты");

                SelectTitle.WaitElementDisplayed(10);
                // клик в найденный пункт "Контакты"
                var btnListContact = new WebItem("//div[@title='CRM -> Контакты']", "Список контактов");
                
                btnListContact.Click();

               
                
                return new СRMContactsPage();
            }
        }
    }
}