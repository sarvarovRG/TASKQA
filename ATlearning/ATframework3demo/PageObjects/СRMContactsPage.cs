﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class СRMContactsPage
    {
        internal void CheckNewContact()
        {
            var CheckNewContact = new WebItem($"//a[contains(text(), 'New contact') and contains(@target, '_top')]", 
                $"Ссылка на контакт 'New contact' в гриде");


            if (CheckNewContact.WaitElementDisplayed())
            {
                Log.Info("Контакт добавился");

            }
            else
            {
                Log.Error("Контакт не отобразился");
            }
        }
    }
}