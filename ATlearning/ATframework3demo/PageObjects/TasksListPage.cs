﻿using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    public class TasksListPage
    {



        internal NewTask addTask()
        {
            // нажимем добавить задачу
            
            
            var btnAddTask = new WebItem("//a[@id='tasks-buttonAdd']", "Кнопка добавления задачи");
            btnAddTask.Click();
            
            var sliderFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Фрейм слайдера");
            sliderFrame.SwitchToFrame();

            return new NewTask();
        }

        internal selectedTaskPage selectTask()
        { // выбор созданной задачи
            // в поиск вводим название задачи
            DriverActions.SwitchToDefaultContent();
            var SelectTitle = new WebItem($"//a[contains(text(), 'Задача на проверку крайнего срока') and contains(@class, 'task-title')]", "Задача в списке др. задач (грид)");

            SelectTitle.WaitElementDisplayed();
            // клик в найденную задачу
            
            SelectTitle.Click();

            return new selectedTaskPage();

        }
    }
} 