﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    internal class CRMListPage
    {
        internal NewCRMcontact addNewContact()
        { // навести курсор на  "клиенты"  
            
            
            var btnCustomersCRM = new WebItem("//span[contains(text(),'Клиенты')]", "навести курсор на клиенты");
            btnCustomersCRM.Hover();

            btnCustomersCRM.WaitElementDisplayed(1);

            var btnContactCRM = new WebItem("//span[contains(text(),'Клиенты')]", "клик на контакты");
            btnContactCRM.Click();

            return new NewCRMcontact();
        }
    }
}