﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;

namespace ATframework3demo.TestCases
{
    public class Case_Bitrix24_TaskDeadline : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var caseCollection = new List<TestCase>();
            caseCollection.Add(new TestCase("Изменение крайнего срока задачи", homePage => NewTaskDeadline(homePage)));

            return caseCollection;
        }

        void NewTaskDeadline(atFrameWork2.PageObjects.PortalHomePage homePage)
        {

            homePage
            .LeftMenu
            .OpenTasks() // перейти во вкладку задачи и проекты
            .addTask()// нажать добавить
            .NameTask() // ввести название задачи
            .saveTask();// нажаить сохранить задачу

                

            bool СhangeDeadlineTask = homePage
            .LeftMenu
            .CheckTask()    
            .selectTask() // выбрать созданную задачу
            .EditingDeadline() // поменять крайний срок на любую дату
            .CheckDeadline("Нет"); // проверить поменялся крайний срок или нет

            if (СhangeDeadlineTask)
            {
                
               
                Log.Error("Крайний срок не поменялся!");

            }
            else
            {
                Log.Info("Крайний срок поменялся!");
            }


        }

    }
}
