﻿using atFrameWork2.BaseFramework;
using atFrameWork2.PageObjects;

namespace ATframework3demo.TestCases
{
    
    
        public class Case_Bitrix24_CRMadd : CaseCollectionBuilder
        {
            protected override List<TestCase> GetCases()
            {
                var caseCollection = new List<TestCase>();
                
                caseCollection.Add(new TestCase("Добавление нового контакта CRM", homePage => AddContactToCRM(homePage)));
               
                return caseCollection;
            }

        void AddContactToCRM(atFrameWork2.PageObjects.PortalHomePage homePage)
        {
            
            homePage
                .Find
                .FindContactCRM()                    //найдем и перейдем на страниу контактов
                .addNewContact()                // переход во форму добавления нового контакта
                .WriteNameContact("New contact") // ввод имени нового контакта
                .SaveContact()                      // сохранить
                .СloseContactFrame()                    // закрыть
                .CheckNewContact();             // cозданный контакт и отчистить список контактов
            
        }
    }



}

